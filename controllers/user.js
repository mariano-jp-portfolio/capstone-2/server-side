const User = require('../models/user');
const Course = require('../models/course');
const bcrypt = require('bcryptjs');
const auth = require('../auth');

module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false;
	});
};

module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
	});

	return user.save().then((user, err) => {
		return (err) ? false : true;
	});
};

module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then(user => {
		if (user === null) { return false }

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password);

		if (isPasswordMatched) {
			return { accessToken: auth.createAccessToken(user.toObject()) };
		} else {
			return false;
		}
	});
};

// User Profile
module.exports.get = (params) => {
	return User.findById(params.userId)
	.then(user => {
		user.password = undefined;
		return user;
	});
};

// Fetch user's name via userId
module.exports.getName = (params) => {
	return User.findById(params.userId).then(user => user)
};

module.exports.enroll = (params) => {
	return User.findById(params.userId).then(user => {
		user.enrollments.push({ courseId: params.courseId });

		return user.save().then((user, err) => {
			return Course.findById(params.courseId).then(course => {
				course.enrollees.push({ userId: params.userId });

				return course.save().then((course, err) => {
					return (err) ? false : true;
				});
			});
		});
	});
};

// Update names, email, and mobile number of the user
module.exports.updateProfile = (params, data) => {
	let updates = {
		firstName: data.firstName,
		lastName: data.lastName,
		email: data.email,
		mobileNo: data.mobileNo
	};
	
	return User.findByIdAndUpdate(params.userId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// Update first and last name of the user
module.exports.updateProfileNames = (params, data) => {
	let updates = {
		firstName: data.firstName,
		lastName: data.lastName
	};
	
	return User.findByIdAndUpdate(params.userId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// Update email of the user
module.exports.updateProfileEmail = (params, data) => {
	let updates = {
		email: data.email
	};
	
	return User.findByIdAndUpdate(params.userId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// Update mobile number of the user
module.exports.updateProfileNum = (params, data) => {
	let updates = {
		mobileNo: data.mobileNo
	};
	
	return User.findByIdAndUpdate(params.userId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

module.exports.changePassword = (params) => {
	
};

module.exports.verifyGoogleTokenId = (params) => {
	
};