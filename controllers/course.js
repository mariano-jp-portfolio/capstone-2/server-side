const Course = require('../models/course');

module.exports.getAll = () => {
	return Course.find({ isActive: true }).then(courses => courses);
};

// To display an inactive course to the Admin
module.exports.getInactive = () => {
	return Course.find({ isActive: false }).then(courses => courses);
};

module.exports.add = (params) => {
	let course = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	});

	return course.save().then((course, err) => {
		return (err) ? false : true;
	});
};

module.exports.get = (params) => {
	return Course.findById(params.courseId).then(course => course);
};

// Editing a course (name, description, and price)
module.exports.update = (params, data) => {
	let updates = {
		name: data.name,
		description: data.description,
		price: data.price
	};

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// Editing a course's name
module.exports.updateName = (params, data) => {
	let updates = {
		name: data.name
	};

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// Editing a course's desc
module.exports.updateDesc = (params, data) => {
	let updates = {
		description: data.description
	};

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// Editing a course's price
module.exports.updatePrice = (params, data) => {
	let updates = {
		price: data.price
	};

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// Editing a course's name and description
module.exports.updateNameDesc = (params, data) => {
	let updates = {
		name: data.name,
		description: data.description
	};

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// Editing a course's name and price
module.exports.updateNamePrice = (params, data) => {
	let updates = {
		name: data.name,
		price: data.price
	};

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// Editing a course's description and price
module.exports.updateDescPrice = (params, data) => {
	let updates = {
		description: data.description,
		price: data.price
	};

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// Archive a course
module.exports.archive = (params) => {
	const updates = { isActive: false };

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// Reactivating an inactive course
module.exports.reactivate = (params) => {
	const updates = { isActive: true };

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true;
	});
};

// List all of the enrolled students in a course
module.exports.enrollees = (params) => {
	return Course.findById(params.courseId).then(course => {
		return course;
	});
};