const express = require('express');
const router = express.Router();
const auth = require('../auth');
const CourseController = require('../controllers/course');

router.get('/', (req, res) => {
    CourseController.getAll().then(courses => res.send(courses));
})

// To display an inactive course to the Admin
router.get('/inactive', (req, res) => {
	CourseController.getInactive().then(courses => res.send(courses));
})

router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId;
    CourseController.get({ courseId }).then(course => res.send(course));
})

router.post('/', auth.verify, (req, res) => {
    CourseController.add(req.body).then(result => res.send(result));
})

// Editing a course (name, description, and price)
// Able to fix this by passing in two arguments, {courseId} and req.body 
router.put('/edit/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
	CourseController.update({ courseId }, req.body).then(result => res.send(result));
})

// Editing a course's name
router.put('/edit/name/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
	CourseController.updateName({ courseId }, req.body).then(result => res.send(result));
})

// Editing a course's description
router.put('/edit/desc/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
	CourseController.updateDesc({ courseId }, req.body).then(result => res.send(result));
})

// Editing a course's price
router.put('/edit/price/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
	CourseController.updatePrice({ courseId }, req.body).then(result => res.send(result));
})

// Editing a course's name and description
router.put('/edit/name_desc/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
	CourseController.updateNameDesc({ courseId }, req.body).then(result => res.send(result));
})

// Editing a course's name and price
router.put('/edit/name_price/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
	CourseController.updateNamePrice({ courseId }, req.body).then(result => res.send(result));
})

// Editing a course's description and price
router.put('/edit/desc_price/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
	CourseController.updateDescPrice({ courseId }, req.body).then(result => res.send(result));
})

// Archive a course
router.delete('/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
    CourseController.archive({ courseId }).then(result => res.send(result));
})

// Reactivating an inactive course
// Able to fix this business process by adding the /reactivate route
router.put('/reactivate/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
	CourseController.reactivate({ courseId }).then(result => res.send(result));
})

// List all of the enrolled students in a course
router.get('/enrollees/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
	CourseController.enrollees({ courseId }).then(course => res.send(course));
})

module.exports = router;