const express = require('express');
const router = express.Router();
const auth = require('../auth');
const UserController = require('../controllers/user');

// [SECTION] Primary Routes

router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result));
})

router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result));
})

router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result));
})

// Fetch user's information
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
    UserController.get({ userId: user.id }).then(user => res.send(user));
})

// Fetch user's name via userId
router.get('/details/:userId', (req, res) => {
    const userId = req.params.userId;
    UserController.getName({ userId }).then(user => res.send(user));
})

router.post('/enroll', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	};
    UserController.enroll(params).then(result => res.send(result));
})

// Update names, email, and mobile number of the user
router.put('/update', auth.verify, (req, res) => {
    // const userId = req.params.userId;
    const user = auth.decode(req.headers.authorization);
    UserController.updateProfile({ userId: user.id }, req.body).then(result => res.send(result));
})

// Update first and last name of the user
router.put('/update/names', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    UserController.updateProfileNames({ userId: user.id }, req.body).then(result => res.send(result));
})

// Update email of the user
router.put('/update/email', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    UserController.updateProfileEmail({ userId: user.id }, req.body).then(result => res.send(result));
})

// Update mobile number of the user
router.put('/update/num', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    UserController.updateProfileNum({ userId: user.id }, req.body).then(result => res.send(result));
})

router.put('/change-password', (req, res) => {
    UserController.changePassword();
})

// [SECTION] Integration Routes

router.post('/verify-google-id-token', (req, res) => {
    UserController.verifyGoogleTokenId();
})

module.exports = router;